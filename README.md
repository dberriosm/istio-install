# Instalación de Istio en Minikube y GKE.

Instalación base de istio versión 1.8.4 en Minikube y GKE con grafana, kiali, prometheus.
Se genera pipeline con stages Install y Validate para instalacion de Istio en base a la documentacion oficial de Istio.

Documentacion Instalacion Istio -> https://istio.io/latest/docs/setup/getting-started/#download

Documentacion Istio 1.8.4 -> https://istio.io/latest/news/releases/1.8.x/announcing-1.8.4/

## Variables

A continuacion se describen las variables necesarias para el correcto funcionamiento del pipeline.


| Variable              | Tipo     | Descripcion     |
|-----------------------|----------|-----------------|
| KUBECONFIG_PRD        | File | KUBECONFIG GKE      |
| KUBECONFIG_QA         | File | KUBECONFIG Minikube |
| DEPLOY_SA_PRD         | Variable | JSON Cuenta de Servicio GKE |
| ISTIO_VERSION         | Variable | Version Istio |
| KUBE_VERSION          | Variable | Version kubectl |
| GRAFANA_HOST_GKE      | Variable | Host Grafana GKE |
| GRAFANA_HOST_LOCAL    | Variable | Host Grafana Minikube |
| JAEGER_HOST_GKE       | Variable | Host Jaeger GKE |
| JAEGER_HOST_LOCAL     | Variable | Host Jaeger Minikube |
| KIALI_HOST_GKE        | Variable | Host Kiali GKE |
| KIALI_HOST_LOCAL      | Variable | Host Kiali Minikube |
| PROMETHEUS_HOST_GKE   | Variable | Host Prometheus GKE |
| PROMETHEUS_HOST_LOCAL | Variable | Host Prometheus Minikube |
